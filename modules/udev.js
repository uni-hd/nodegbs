module.exports = function() {
  var _udev = require("udev");
  var emit;

  function udev() {
    this.constructor.apply(this, arguments[0]);
  }

  udev.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;

    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  }

  udev.prototype.init = function() {
    this.devices = [];
    this.monitor = _udev.monitor();
    this.monitor.on('add', function(device) {
      this.addDevice(device);
    }.bind(this));
    this.monitor.on('remove', function(device) {
      this.delDevice(device);
    }.bind(this));
    this.addCurrentDevices();
    emit("udev.init");
  };

  udev.prototype.getDevice = function(vendor, model, serial) {
    var i = this.getDeviceIndex(vendor, model, serial);
    return (i >= 0) ? this.devices[i] : undefined;
  };

  udev.prototype.getDeviceIndex = function(vendor, model, serial) {
    if (vendor != undefined && model != undefined) {
      for (var i = 0; i < this.devices.length; i++) {
        if (this.devices[i].vendor == vendor && this.devices[i].model == model && this.devices[i].serial == serial) {
          return i;
        }
      }
    }
    return -1;
  };

  udev.prototype.getDevices = function() {
    return this.devices;
  };

  udev.prototype.getDevicesWithSettings = function() {
    return {
      "devices": this.getDevices(),
      "receiver": this.getDeviceIndex(this.parent.o.gps.vendor, this.parent.o.gps.model, this.parent.o.gps.serial),
      "transmitter": this.getDeviceIndex(this.parent.o.str2str.vendor, this.parent.o.str2str.model, this.parent.o.str2str.serial)
    };
  };

  udev.prototype.isValid = function(device) {
    return this.isPhysical(device) && this.isTTY(device) && this.isUSB(device);
  };

  udev.prototype.isPhysical = function(device) {
    return (device.DEVNAME !== undefined && device.ID_SERIAL !== undefined && device.ID_VENDOR_ID !== undefined && device.ID_MODEL_ID !== undefined);
  };

  udev.prototype.isTTY = function(device) {
    return (device.SUBSYSTEM !== undefined && device.SUBSYSTEM == "tty");
  };

  udev.prototype.isUSB = function(device) {
    return (device.ID_BUS !== undefined && device.ID_BUS == "usb");
  };

  udev.prototype.getName = function(device) {
    if (device.ID_MODEL_FROM_DATABASE != undefined) {
      return device.ID_MODEL_FROM_DATABASE;
    }
    if (device.ID_MODEL != undefined) {
      return device.ID_MODEL;
    }
    return device.ID_SERIAL;
  };

  udev.prototype.formatName = function(device) {
    return this.getName(device) + " [" + device.DEVNAME + "]";
    //return this.getName(device) + " [" + device.ID_VENDOR_ID + ":" + device.ID_MODEL_ID + "]";
  };

  udev.prototype.generateDevice = function(device) {
    return {
      "dev": device.DEVNAME,
      "port": device.DEVNAME.replace("/dev/", ""),
      "name": this.formatName(device),
      "vendor": device.ID_VENDOR_ID,
      "model": device.ID_MODEL_ID,
      "serial": device.ID_SERIAL
    };
  };

  udev.prototype.addDevice = function(device) {
    if (this.isValid(device)) {
      this.devices.push(this.generateDevice(device));
      emit("basestation.devices.changed");
    }
  };

  udev.prototype.delDevice = function(device) {
    if (this.isValid(device)) {
      for (var i = 0; i < this.devices.length; i++) {
        if (this.devices[i].dev == device.DEVNAME) {
          this.devices.splice(i, 1);
          emit("basestation.devices.changed");
          return;
        }
      }
    }
  };

  udev.prototype.addCurrentDevices = function() {
    dev = _udev.list("tty");
    dev.forEach(function(device) {
      this.addDevice(device);
    }.bind(this));
  };

  return new udev((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}