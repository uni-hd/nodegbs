module.exports = function() {
  var fs = require('fs');
  var exec = require("child_process").exec;
  var utils = require('./utils');

  function basestation() {
    this.constructor.apply(this, arguments[0]);
  }

  basestation.prototype.constructor = function(queuesize) {
    this._events = {};
    this._mode = 0;
    this._lon = 0;
    this._lat = 0;
    this._connection = 0;
    this._alt = 0;
    this._sat = 0;
    this._status = 0;
    this.version = 1;
    this._settingsfile = "config/settings.json";
    this.extend = require("extend");
    this.fs = require("fs");
  }

  basestation.prototype.init = function(options) {
    var defaults = {
      "gps": {
        "port": 2947,
        "bin": "gpsd",
        "pid": "/tmp/gpsd.pid",
        "host": "localhost",
        "watch": true,
        "watchoptions": undefined, // use defaults
        "delay": 1000,
        "baudrate": 115200
      },
      "web": {
        "ip": undefined,
        "port": 80,
        "host": "node.gbs",
        "wwwroot": "public",
        "title": "NodeGBS - Web interface",
        "brand": "NodeGBS"
      },
      "gpio": {
        "boot": 11,
        "status": 12,
        "pulsefrequecy": 2000,
        "reset": 13,
        "resetdelay": 5000
      },
      "wlan": {
        "channel": 6,
        //"driver": "rtl871xdrv",
        "hw_mode": "g",
        "interface": "wlan0",
        "ssid": "NodeGBS AP",
        "wpa": 2,
        "wpa_passphrase": "gpsbaseA"
      },
      "basestation": {
        "queuesize": 1000,
        "pollinterval": 0 // 0 to disbale polling
      },
      "str2str": {
        "cmd": "str2str",
        "in": "serial://PORT:115200:8:n:1:#ubx",
        "out": "tcpsvr://:PORT",
        "port": 1337,
        "config": "config/ubx_m8n_glo_raw_1hz_ext.cmd",
        "stationid": 1337,
        "msg": "1004,1019,1012,1020,1006,1008"
      },
      "udev": {},
      "transmitter": {
        "listen_port": 1337,
        "baudrate": 115200,
        "system": 1,
        "componentID": 1
      }
    };
    var settings;
    try {
      settings = JSON.parse(fs.readFileSync(this._settingsfile, "utf8"));
    } catch (e) {
      settings = {};
    }
    this.o = this.extend(true, defaults, settings);
    this.o = this.extend(true, this.o, options);
    console.log(this.o);

    this._gpio = require('./gpio')(this, this.o.gpio);
    this.on("gpio.init", function(port) {
      console.log("GPIO running.");
    }.bind(this));
    this._gpio.init();

    this._gps = require('./gps')(this, this.o.gps);
    this._web = require('./web')(this, this.o.web);
    this._wlan = require('./wlan')(this, this.o.wlan);
    this._queue = require("./queue")(this, this.o.basestation.queuesize);
    this._str2str = require("./str2str")(this, this.o.str2str);
    this._udev = require("./udev")(this, this.o.udev);
    this._transmitter = require("./transmitter")(this, this.o.transmitter);


    this.on("wlan.init", function(err, name) {
      if (err) {
        console.log("Access point could not be started!");
        this.gpio().error(true);
        return;
      }
      console.log("Access point started with name \"" + name + "\"");
      this.gpio().boot(false);
      this.gpio().pulse(true);
      //this.gpio().blink(false);
      //this.gpio().status(true);
    }.bind(this));
    this._wlan.init();

    this.on("udev.init", function() {
      console.log("udev initialized");
    });
    this._udev.init();

    this.on("gps.daemon.init", function(port) {
      console.log("gpsd running on port " + port + "...");
    });

    this.on("gps.listener.init", function(port) {
      console.log("gpsd listener running on port " + port + "...");
      if (this.o.basestation.pollinterval > 0) {
        this.pollinterval = setInterval(function() {
          console.log("poll");
          this.gps().poll();
        }.bind(this), this.o.basestation.pollinterval);
        this._gps.on("POLL", function() {
          this.handlePOLL.apply(this, arguments);
        }.bind(this));
      } else {
        this._gps.on("TPV", function() {
          this.handleTPV.apply(this, arguments);
        }.bind(this));
        this._gps.on("SKY", function() {
          this.handleSKY.apply(this, arguments);
        }.bind(this));
      }
    }.bind(this));

    this.on("web.init", function(addr) {
      console.log("Web interface running on http://" + addr.address + ":" + addr.port + "");
    }.bind(this));
    this._web.init();

    this.on("basestation.wlan.changed", function() {
      console.log("wlan changed");
      this.wlan().reload(this.o.wlan);
    }.bind(this));

    this.on("basestation.devices.receiver.changed", function() {
      if (this.mode() > 0) {
        this.gps().reload(this.o.gps);
      }
      this.str2str().reload(this.o.str2str, function() {
        this.transmitter().reload(this.o.transmitter);
      }.bind(this));
    }.bind(this));

    this.on("basestation.devices.transmitter.changed", function() {
      this.str2str().reload(this.o.str2str, function() {
        this.transmitter().reload(this.o.transmitter);
      }.bind(this));
    }.bind(this));

    this.on("basestation.mode.changed", function() {
      switch (this.mode()) {
        case 0:
          this.gps().stop();
          break;
        case 1:
          this.gps().init();
          break;
        default:
          console.log("U broke it!");
      }
    }.bind(this));

    this.on("basestation.restart", function() {
      console.log("RESTART");
      // TODO: Restart server
    }.bind(this));

    return this;
  };

  basestation.prototype.reset = function() {
    console.log("RESET");
    try {
      fs.unlinkSync(this._settingsfile);
    } catch (e) {

    }
    setTimeout(function() {
      exec("systemctl restart nodegbs", function() {});
    }, 250);
  };

  basestation.prototype.setting = function(settings, extend) {
    var towrite = {};
    if (extend !== false) {
      try {
        towrite = JSON.parse(fs.readFileSync(this._settingsfile, "utf8"));
      } catch (e) {
        towrite = {};
      }
    }
    this.o = this.extend(true, this.o, settings);
    towrite = this.extend(true, towrite, settings);
    try {
      fs.writeFileSync(this._settingsfile, JSON.stringify(towrite), "utf8")
    } catch (e) {

    }
  };

  basestation.prototype.on = function(name, listener) {
    if (!this._events[name]) {
      this._events[name] = [];
    }
    this._events[name].push(listener);
  };

  basestation.prototype.emit = function(name) {
    if (!!name && !!this._events[name]) {
      var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
      for (var i = 0; i < this._events[name].length; i++) {
        this._events[name][i].apply(this, args.slice(1));
      }
    }
  };

  basestation.prototype.gps = function() {
    return this._gps;
  };

  basestation.prototype.queue = function() {
    return this._queue;
  };

  basestation.prototype.web = function() {
    return this._web;
  };

  basestation.prototype.wlan = function() {
    return this._wlan;
  };

  basestation.prototype.gpio = function() {
    return this._gpio;
  };

  basestation.prototype.str2str = function() {
    return this._str2str;
  };

  basestation.prototype.udev = function() {
    return this._udev;
  };

  basestation.prototype.transmitter = function() {
    return this._transmitter;
  };

  // Mode (0 -> Known position, 1 -> Unknown position)
  basestation.prototype.mode = function(m) {
    if (m !== undefined) {
      var old = this.mode();
      this._mode = utils.toInt(m, this.mode);
      if (old !== this.mode()) {
        this.emit("basestation.mode.changed");
        this.emit("basestation.status.changed");
      }
    }
    return this._mode;
  };

  basestation.prototype.sending = function(state) {
    if (state !== undefined) {
      if (state == true) {
        this.str2str().start(function() {
          this.transmitter().start();
        }.bind(this));
      } else {
        this.str2str().stop(function() {
          this.transmitter().stop();
        }.bind(this));
      }
      this.emit("basestation.sending.changed");
    }
    return this.str2str().running();
  };

  // Longitude
  basestation.prototype.lon = function(lon) {
    if (lon !== undefined) {
      this._lon = utils.toFloat(lon, this.lon);
    }
    return (this.mode() === 0) ? this._lon : this.queue().median().lon;
  };

  // Latitude
  basestation.prototype.lat = function(lat) {
    if (lat !== undefined) {
      this._lat = utils.toFloat(lat, this.lat);
    }
    return (this.mode() === 0) ? this._lat : this.queue().median().lat;
  };

  // Altitude
  basestation.prototype.alt = function(alt) {
    if (alt !== undefined) {
      this._alt = utils.toFloat(alt, this.alt);
    }
    return (this.mode() === 0) ? this._alt : this.queue().median().alt;
  };

  // Location (if mode 1 locations are added to queue instead of local variables)
  basestation.prototype.loc = function(lon, lat, alt) {
    var old = {
      "lon": this.lon(),
      "lat": this.lat(),
      "alt": this.alt()
    };
    if (lon !== undefined && lat !== undefined && alt !== undefined) {
      if (this.mode() === 0) {
        this.lon(lon);
        this.lat(lat);
        this.alt(alt);
      } else if (this.mode() === 1) {
        this.queue().add({
          "lon": lon,
          "lat": lat,
          "alt": alt
        });
      } else {
        console.error("Invalid mode");
      }
      var newloc = this.loc();
      if (old.lon != newloc.lon || old.lat != newloc.lat || old.alt != newloc.alt) {
        old = newloc;
        this.emit("basestation.location.changed");
      }
    }
    return old;
  };

  // Satellites
  basestation.prototype.sat = function(sat) {
    if (sat !== undefined) {
      var old = this.sat();
      this._sat = utils.toInt(sat, this.sat);
      if (old !== this.sat()) {
        this.emit("basestation.status.changed");
      }
    }
    return this._sat;
  };

  basestation.prototype.connection = function(c) {
    if (c !== undefined && this.mode() == 1) {
      var old = this.connection();
      this._connection = c;
      if (old != this.connection()) {
        this.emit("basestation.status.changed");
      }
    }
    return this._connection;
  };

  basestation.prototype.status = function() {
    return {
      "mode": this.mode(),
      "conn": this.connection(),
      "sat": this.sat(),
      "fix": this.queue().size(),
      "maxfix": this.queue()._size
    };
  };

  basestation.prototype.handleTPV = function(tpv) {
    if (tpv != undefined && tpv.mode != undefined) {
      //console.log(tpv);
      switch (this.connection(tpv.mode)) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
        case 3:
          if (tpv.lon != undefined && tpv.lat != undefined && tpv.alt != undefined && this.mode() == 1) {
            this.loc(tpv.lon, tpv.lat, tpv.alt);
          }
          break;
        default:
          // How did that happen?
      }
    }
  };

  basestation.prototype.handleSKY = function(sky) {
    if (sky != undefined && sky.satellites != undefined && this.mode() == 1) {
      //console.log(sky);
      // TODO: Use satellites.map(it.used == true)
      this.sat(sky.satellites.length);
    }
  };

  basestation.prototype.handlePOLL = function(poll) {
    if (poll == undefined || this.mode() != 1) {
      return;
    }
    if (poll.tpv !== undefined) {
      this.handleTPV(poll.tpv);
    }
    if (poll.sky !== undefined) {
      this.handleSKY(poll.sky);
    }
  };

  return new basestation((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
