function utils() {}

utils.prototype.toInt = function(v, fallback) {
  return parseInt(this.toFloat(v, fallback));
}

utils.prototype.toFloat = function(v, fallback) {
  if (v === undefined || v === null || v == "") {
    return 0;
  }
  if (typeof v === "string") {
    var f = parseFloat(v);
    return (f === NaN) ? fallback() : f;
  } else if (typeof v === "number") {
    return parseFloat(v);
  }
  return fallback();
}

module.exports = new utils();
