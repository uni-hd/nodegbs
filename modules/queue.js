module.exports = function() {
  var emit;

  function queue() {
    this.constructor.apply(this, arguments[0]);
  }

  queue.prototype.constructor = function(parent, size) {
    this.parent = parent;
    this.elements = [];
    this.elements.length = size;
    this.pointer = 0;
    this._size = size;
    this.changed = true;
    this._median = undefined;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  queue.prototype.add = function(loc) {
    this.elements[this.pointer++] = loc;
    if (this.pointer >= this._size) {
      this.pointer = 0;
    }
    this.changed = true;
    emit("basestation.status.changed");
  }

  queue.prototype.print = function() {
    var e;
    process.stdout.write("[");
    for (var i = 0; i < this._size; i++) {
      e = this.elements[i];
      if (e == undefined) {
        process.stdout.write("undefined");
      } else {
        process.stdout.write("{\"lon\":" + e.lon + ",\"lat\":" + e.lat + ",\"alt\":" + e.alt + "}");
      }
      if (i < this._size - 1) {
        process.stdout.write(",");
      }
    }
    process.stdout.write("]\n");
  }

  queue.prototype.median = function() {
    if (this.changed === false) {
      return this._median;
    }
    var getmiddleofarray = function(a) {
      if (a.length <= 0) {
        return 0;
      }
      if (a.length % 2 == 0) {
        return (a[Math.floor(a.length / 2) - 1] + a[Math.floor(a.length / 2)]) / 2;
      }
      return a[Math.floor(a.length / 2)];
    }
    var lon = [];
    var lat = [];
    var alt = [];
    var e;
    for (var i = 0; i < this._size; i++) {
      e = this.elements[i];
      if (e == undefined) {
        continue;
      }
      lon.push(e.lon);
      lat.push(e.lat);
      alt.push(e.alt);
    }
    if (this._median != undefined) {
      lon.push(this._median.lon);
      lat.push(this._median.lat);
      alt.push(this._median.alt);
    }
    lon.sort(function(a, b) {
      return a - b;
    });
    lat.sort(function(a, b) {
      return a - b;
    });
    alt.sort(function(a, b) {
      return a - b;
    });
    this._median = {
      "lon": getmiddleofarray(lon),
      "lat": getmiddleofarray(lat),
      "alt": getmiddleofarray(alt)
    };
    this.changed = false;
    return this._median;
  };

  queue.prototype.size = function() {
    var size = 0;
    for (var i = 0; i < this._size; i++) {
      if (this.elements[i] != undefined) {
        size++;
      }
    }
    return size;
  };

  queue.prototype.clear = function() {
    for (var i = 0; i < this._size; i++) {
      this.elements[i] = undefined;
    }
    this.changed = true;
  };

  return new queue((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}