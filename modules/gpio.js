module.exports = function() {

  function gpio() {
    this.constructor.apply(this, arguments[0]);
  }

  gpio.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    this.pulsevalue = 0;
    this.pulsemax = 1024;
    this.pulsedirection = 1;
    this.pulseinterval = 50;
    this.pulsestep = this.pulsemax / (this.o.pulsefrequecy / this.pulseinterval);
    this.pulsetimer = undefined;
    this.resetstate = false;
    this.resettimer = undefined;
    this.errortimer = undefined;
    this.errorinterval = 1000;
    this.errorstate = false;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  gpio.prototype.init = function() {
    this.rpio = undefined;
    try {
      this.rpio = require('rpio');
    } catch (e) {
      // Not on an raspberry pi
      console.log("Not on a raspberry pi.", e);
    }
    if (this.rpio != undefined) {
      this.rpio.init({
        gpiomem: false,
        mapping: "physical"
      });

      this.rpio.open(this.o.boot, this.rpio.OUTPUT, this.rpio.LOW);

      this.rpio.open(this.o.status, this.rpio.PWM);
      this.rpio.pwmSetClockDivider(8);
      this.rpio.pwmSetRange(this.o.status, this.pulsemax);

      this.rpio.open(this.o.reset, this.rpio.INPUT, this.rpio.PULL_DOWN);
      this.rpio.poll(this.o.reset, function() {
        this.resetstate = this.rpio.read(this.o.reset);
        console.log("button " + (this.resetstate ? "pressed" : "released"));
        if (this.resetstate) {
          if (this.resettimer) {
            clearTimeout(this.resettimer);
            this.resettimer = undefined;
          }
          this.resettimer = setTimeout(function() {
            if (this.resetstate) {
              this.pulse(false);
              this.boot(true);
              this.parent.reset();
            }
          }.bind(this), this.o.resetdelay);
        } else {
          if (this.resettimer) {
            clearTimeout(this.resettimer);
            this.resettimer = undefined;
          }
        }
      }.bind(this));
      emit("gpio.init");
    }
    return this;
  };

  gpio.prototype.pulse = function(state) {
    if (this.rpio == undefined) {
      return;
    }
    if (state) {
      console.log("pulseing on");
      this.pulsetimer = setInterval(function() {
        //console.log((new Date()).getTime(), this.pulsevalue, this.pulsedirection, this.pulsestep);
        this.pulsevalue = (this.pulsevalue + (this.pulsedirection * this.pulsestep));
        if (this.pulsevalue >= this.pulsemax) {
          this.pulsevalue = this.pulsemax;
          this.pulsedirection *= -1;
        } else if (this.pulsevalue < 0) {
          this.pulsevalue = 0;
          this.pulsedirection *= -1;
        }
        this.rpio.pwmSetData(this.o.status, this.pulsevalue);
      }.bind(this), this.pulseinterval);
    } else {
      console.log("pulseing off");
      if (this.pulsetimer) {
        clearInterval(this.pulsetimer);
        this.pulsetimer = undefined;
      }
    }
  };

  gpio.prototype.error = function(state) {
    if (this.rpio == undefined) {
      return;
    }
    if (state) {
      this.errortimer = setInterval(function() {
        this.boot(this.errorstate);
        this.errorstate = !this.errorstate;
      }.bind(this), this.errorinterval);
    } else {
      if (this.errortimer) {
        clearInterval(this.errortimer);
        this.errortimer = undefined;
      }
    }
  };

  gpio.prototype.boot = function(state) {
    if (this.rpio == undefined) {
      return;
    }
    console.log("boot " + (state ? "on" : "off"));
    this.rpio.write(this.o.boot, state ? this.rpio.LOW : this.rpio.HIGH);
  };

  gpio.prototype.status = function(state) {
    if (this.rpio == undefined) {
      return;
    }
    console.log("status " + (state ? "on" : "off"));
    this.rpio.write(this.o.status, state ? this.rpio.HIGH : this.rpio.LOW);
  };

  return new gpio((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
