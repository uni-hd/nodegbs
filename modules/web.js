module.exports = function() {
  var express = require("express");
  var socketio = require("socket.io");
  var fs = require("fs");
  var utils = require('./utils');
  var emit;

  var walkSync = function(dir, filelist) {
    if (dir[dir.length - 1] != "/") {
      dir = dir.concat("/");
    }
    var fs = fs || require("fs");
    var files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function(file) {
      if (fs.statSync(dir + file).isDirectory()) {
        filelist = walkSync(dir + file + "/", filelist);
      } else {
        filelist.push(dir + file);
      }
    });
    return filelist;
  };

  function web() {
    this.constructor.apply(this, arguments[0]);
  }

  web.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    this.app = undefined;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  web.prototype.app = function() {
    return this.app;
  };

  web.prototype.init = function() {
    this.app = express();
    this.server = require("http").Server(this.app);

    this.app.set("view engine", "pug");

    this.app.use(express.static(this.o.wwwroot));

    this.app.get("/", function(req, res) {
      var hostname = undefined;
      if (req.headers && req.headers["host"]) {
        hostname = (req.headers.host.match(/:/g)) ? req.headers.host.slice(0, req.headers.host.indexOf(":")) : req.headers.host;
      }
      if (hostname !== undefined && hostname == this.o.host) {
        res.render("index", {
          title: this.o.title,
          brand: this.o.brand
        });
      } else {
        res.render("redirect", {
          host: this.o.host
        });
      }
    }.bind(this));

    this.app.get("/app.manifest", function(req, res) {
      res.setHeader("content-type", "text/cache-manifest");
      var manifest = [
        "CACHE MANIFEST",
        "",
        "# Version " + this.parent.version,
        "",
        "CACHE:",
        "/",
        "/socket.io/socket.io.js"
      ];
      walkSync(this.o.wwwroot).forEach(function(file) {
        manifest.push(file.substr(this.o.wwwroot.length));
      }.bind(this));
      [
        "",
        "NETWORK:",
        "*"
      ].forEach(function(entry) {
        manifest.push(entry);
      }.bind(this));
      res.end(manifest.join("\n"));
    }.bind(this));

    this.io = new socketio(this.server);

    this.io.on("connection", function(socket) {
      emit("web.socket.connect");

      socket.on("update", function() {
        socket.emit("update status", this.parent.status());
        socket.emit("update sending", this.parent.sending());
        socket.emit("update location", this.parent.loc());
        socket.emit("update devices", this.parent.udev().getDevicesWithSettings());
      }.bind(this));

      socket.on("update mode", function(mode) {
        this.parent.mode(mode);
      }.bind(this));

      socket.on("update sending", function(state) {
        var s = utils.toInt(state);
        var result = false;
        if (s == 2) {
          this.parent.sending(0);
          result = this.parent.sending(1);
        } else {
          result = this.parent.sending(s === 1);
        }
        if (result != (s > 0)) {
          socket.emit("error str2str");
        }
      }.bind(this));

      socket.on("select receiver", function(device) {
        var vendor = "";
        if (device.vendor != "undefined") {
          vendor = device.vendor;
        }
        var model = "";
        if (device.model != "undefined") {
          model = device.model;
        }
        var serial = "";
        if (device.serial != "undefined") {
          serial = device.serial;
        }
        var changed = false;
        if (this.parent.o.gps.vendor != vendor) {
          changed = true;
        }
        if (!changed && this.parent.o.gps.model != model) {
          changed = true;
        }
        if (!changed && this.parent.o.gps.serial != serial) {
          changed = true;
        }
        if (changed) {
          this.parent.setting({
            "gps": {
              "vendor": vendor,
              "model": model,
              "serial": serial
            }
          });
          emit("basestation.devices.receiver.changed");
        }
      }.bind(this));

      socket.on("select transmitter", function(device) {
        var vendor = "";
        if (device.vendor != "undefined") {
          vendor = device.vendor;
        }
        var model = "";
        if (device.model != "undefined") {
          model = device.model;
        }
        var serial = "";
        if (device.serial != "undefined") {
          serial = device.serial;
        }

        var changed = false;
        if (this.parent.o.transmitter.vendor != vendor) {
          changed = true;
        }
        if (!changed && this.parent.o.transmitter.model != model) {
          changed = true;
        }
        if (!changed && this.parent.o.transmitter.serial != serial) {
          changed = true;
        }
        if (changed) {
          this.parent.setting({
            "transmitter": {
              "vendor": vendor,
              "model": model,
              "serial": serial
            }
          });
          emit("basestation.devices.transmitter.changed");
        }
      }.bind(this));

      socket.on("select protocol", function(protocol) {
        protocol = utils.toInt(protocol);
        if (protocol != this.parent.o.transmitter.protocol) {
          this.parent.setting({
            "transmitter": {
              "protocol": protocol
            }
          });
          this.parent.transmitter().o.protocol = protocol;
        }
      }.bind(this));

      socket.on("update location", function(loc) {
        this.parent.loc(loc.lon, loc.lat, loc.alt);
      }.bind(this));

      socket.on("update password", function(password) {
        if (!this.parent.wlan().validPassword(password) || this.parent.o.wlan.wpa_passphrase == password) {
          return;
        }
        this.parent.setting({
          "wlan": {
            "wpa_passphrase": password
          }
        });
        this.io.emit("restart");
        emit("basestation.restart");
      }.bind(this));

      socket.on("disconnect", function() {
        emit("web.socket.disconnect");
      }.bind(this));
    }.bind(this));

    this.parent.on("basestation.status.changed", function() {
      this.io.emit("update status", this.parent.status());
    }.bind(this));

    this.parent.on("basestation.sending.changed", function() {
      this.io.emit("update sending", this.parent.sending());
    }.bind(this));

    this.parent.on("basestation.location.changed", function() {
      console.log(1);
      this.io.emit("update location", this.parent.loc());
      console.log(2);
    }.bind(this));

    this.parent.on("basestation.devices.changed", function() {
      this.io.emit("update devices", this.parent.udev().getDevicesWithSettings());
    }.bind(this));

    this.server.listen(this.o.port, this.o.ip, function() {
      emit("web.init", this.server.address());
    }.bind(this));

    return this;
  };

  return new web((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
