module.exports = function() {
  var spawn = require("child_process").spawn;
  var fs = require("fs");
  var emit;

  function gpsd() {
    this.constructor.apply(this, arguments[0]);
  }

  gpsd.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    this._daemon = undefined;

    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  gpsd.prototype.init = function() {
    this.device = this.parent.udev().getDevice(this.o.vendor, this.o.model, this.o.serial);
  };

  gpsd.prototype.start = function(cb) {
    console.log("daemon dev:", this.device);
    if (this.device !== undefined) {
      fs.exists(this.device.dev, function(exists) {
        if (exists) {
          if (this._daemon == undefined) {
            this.arguments = [
              "-N",
              "-P", this.o.pid,
              "-S", this.o.port,
              this.device.dev
            ];

            if (this.o.readOnly) {
              this.arguments.push("-b");
            }

            console.log("Start the daemon with spawn");

            this._daemon = spawn(this.o.bin, this.arguments);
            this._daemon.on("exit", function(code) {
              emit("gps.daemon.exit");
              this._daemon = undefined;
            });

            this._daemon.on("error", function(err) {
              emit("gps.daemon.error", err);
            });

            // give the daemon a change to startup before makeing the callback.
            setTimeout(function() {
              if (cb !== undefined) {
                cb.call();
              }
            }, 1000);
          }
        } else {
          emit("gps.daemon.error", this.device.dev + " does not exist.");
        }
      }.bind(this));
    } else {
      emit("gps.daemon.error", "receiver device not specified.");
    }
  };

  gpsd.prototype.stop = function(cb) {
    if (this._daemon !== undefined) {
      this._daemon.on("exit", function(code) {
        if (cb !== undefined) {
          cb.call();
        }
      });
      this._daemon.kill();
      this._daemon = undefined;
    } else if (cb !== undefined) {
      cb.call();
    }
  };

  gpsd.prototype.reload = function() {
    this.stop(function() {
      this.init();
      this.start(function() {
        emit("gps.daemon.started");
      }.bind(this));
    }.bind(this));
  };

  return new gpsd((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
