module.exports = function() {
  var spawn = require("child_process").spawn;
  var emit;

  function str2str() {
    this.constructor.apply(this, arguments[0]);
  }

  str2str.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    this.server = undefined;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  str2str.prototype.start = function(cb) {
    if (this.running()) {
      return false;
    }
    var receiver = this.parent.udev().getDevice(this.parent.o.gps.vendor, this.parent.o.gps.model, this.parent.o.gps.serial);
    if (receiver != undefined && receiver.port != undefined) {
      receiver = receiver.port;
    } else {
      return false;
    }
    //		var transmitter = this.parent.udev().getDevice(this.o.vendor, this.o.model, this.o.serial);
    //		if (transmitter != undefined && transmitter.port != undefined) {
    //			transmitter = transmitter.port;
    //		} else {
    //			return false;
    //		}
    var loc = this.parent.loc();

    arguments = [
      "-in", this.o.in.replace("PORT", receiver),
      "-out", this.o.out.replace("PORT", this.o.port),
      "-c", this.o.config,
      "-p", loc.lon, loc.lat, loc.alt,
      "-sta", this.o.id,
      "-msg", this.o.msg
    ];
    command = this.o.cmd;
    console.trace(command, arguments);
    this.server = spawn(command, arguments);
    if (cb !== undefined)  {
      setTimeout(cb, 500);
    }
    return true;
  };

  str2str.prototype.stop = function(cb) {
    if (this.running()) {
      this.server.kill("SIGINT");
      this.server = undefined;
      if (cb !== undefined) {
        setTimeout(cb, 500);
      }
      return true;
    }
    if (cb !== undefined) {
      cb.call();
    }
    return false;
  };

  str2str.prototype.reload = function(options, cb) {
    var running = this.stop(function() {
      this.constructor(this.parent, options);
      this.start(cb);
    }.bind(this));
  };

  str2str.prototype.running = function() {
    return this.server != undefined;
  };

  return new str2str((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
