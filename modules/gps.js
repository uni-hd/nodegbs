module.exports = function() {
  var gpsd = require('node-gpsd');
  var emit;

  function gps() {
    this.constructor.apply(this, arguments[0]);
  }

  gps.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    this._daemon = require('./gpsd')(this.parent, this.o);

    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  gps.prototype.daemon = function() {
    return this._daemon;
  };

  gps.prototype.listener = function() {
    return this._listener;
  };

  gps.prototype.on = function(e, cb) {
    this.listener().on(e, function(data) {
      if (data.device != undefined) {
        if (this.device != undefined && data.device == this.device.dev) {
          cb(data);
        }
      } else {
        cb(data);
      }
    }.bind(this));
    console.log("registering", "gps." + e);
    this.parent.on("gps." + e, function(data) {
      if (data.device != undefined) {
        if (this.device != undefined && data.device == this.device.dev) {
          cb(data);
        }
      } else {
        cb(data);
      }
    }.bind(this));
  };

  gps.prototype.poll = function() {
    if (this.listener().isConnected()) {
      this.listener().serviceSocket.write('?POLL;\n'); // maybe fork gpsd lib to include this in the lib
    }
  };

  gps.prototype.devices = function() {
    if (this.listener().isConnected()) {
      this.listener().devices();
    }
  };

  gps.prototype.init = function() {
    this.device = this.parent.udev().getDevice(this.o.vendor, this.o.model, this.o.serial);
    if (this.daemon() !== undefined) {
      this.daemon().init();
    }
    this._listener = new gpsd.Listener({
      port: this.o.port,
      hostname: this.o.host,
      logger: {
        info: function() {
          emit("gps.log.listener.info", arguments);
        },
        warn: function() {
          emit("gps.log.listener.warn", arguments);
        },
        error: function() {
          emit("gps.log.listener.error", arguments);
        }
      },
      parse: true
    });

    this.listener().on("error", function() {
      emit("gps.listener.error", arguments);
    });

    if (this.daemon() != undefined) {
      console.log("start daemon");
      this.daemon().start(function() {
        emit("gps.daemon.init", this.o.port);
        setTimeout(function() {
          this.listener().connect(function() {
            if (this.o.watch === true) {
              this._listener.watch(this.o.watchoptions);
            }
            emit("gps.listener.init", this.o.port);
          }.bind(this));
        }.bind(this), this.o.delay);
      }.bind(this));
    }
  };

  gps.prototype.disconnect = function(cb) {
    if (this.listener().isConnected()) {
      this.listener().disconnect(cb);
    } else {
      if (cb !== undefined) {
        cb.call();
      }
    }
  }

  gps.prototype.stop = function(cb) {
    if (this.listener() != undefined) {
      this.disconnect(function() {
        this.daemon().stop(function() {
          if (cb !== undefined) {
            cb.call();
          }
        }.bind(this));
      }.bind(this));
    }
  }

  gps.prototype.reload = function(options) {
    console.log("reload gpsd");
    this.stop(function() {
      this.constructor(this.parent, options);
      if (this.daemon() !== undefined) {
        this.daemon().constructor(this.parent, options);
      }
      this.init();
    }.bind(this));
  };

  return new gps((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
