module.exports = function() {
  var hostapd = require('wireless-tools/hostapd');

  function wlan() {
    this.constructor.apply(this, arguments[0]);
  }

  wlan.prototype.constructor = function(parent, options) {
    this.parent = parent;
    this.o = options;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  wlan.prototype.init = function() {
    hostapd.enable(this.o, function(err) {
      emit("wlan.init", err, this.o.ssid);
    }.bind(this));
    return this;
  };

  wlan.prototype.reload = function(options) {
    this.constructor(this.parent, options);
    this.init();
  };

  wlan.prototype.validPassword = function(password) {
    if (password.length < 8) {
      return false;
    }
    return true;
  };

  return new wlan((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}