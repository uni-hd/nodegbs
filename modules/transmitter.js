module.exports = function() {
  var SerialPort = require("serialport");
  var mavlink = require("mavlink");
  var net = require('net');
  var emit;
  const max_len = 180;

  function transmitter() {
    this.constructor.apply(this, arguments[0]);
  }

  transmitter.prototype.constructor = function(parent, options) {
    console.log(options);
    this.parent = parent;
    this.o = options;
    this.mav = undefined;
    this.serial = undefined;
    this.rtcmlisten = undefined;
    emit = function emit() {
      this.parent.emit.apply(this.parent, arguments);
    }.bind(this);
  };

  transmitter.prototype.start = function() {
    this.mav = new mavlink(this.o.system, this.o.componentID);
    var transmitter = this.parent.udev().getDevice(this.o.vendor, this.o.model, this.o.serial);
    console.log("Using transmitter")
    if (transmitter != undefined && transmitter.port != undefined) {
      transmitter = transmitter.dev;
    } else {
      return false;
    }
    this.serial = new SerialPort(transmitter, {
      baudRate: this.o.baudrate
    });
    this.rtcmlisten = new net.Socket();
    this.rtcmlisten.connect(this.o.listen_port, '127.0.0.1', function() {
      console.log("Started TCP Socket connection to port " + this.o.listen_port);
    }.bind(this));
    this.rtcmlisten.on('data', function(data) {
      console.log(this.o.protocol == 0 ? "RTCM3":"Mavlink");
      switch (this.o.protocol) {
        case 0:
          this.send(data);
          break;
        case 1:
          if (data.length < max_len) {
            this.mav.createMessage(
              "GPS_RTCM_DATA", {
                'flag': 0,
                'len': data.length,
                'data': data
              },
              function(message) {
                this.send(message.buffer);
              }.bind(this));
          } else {
            for (var i = 0; i < data.length; i += max_len) {
              this.mav.createMessage(
                "GPS_RTCM_DATA", {
                  'flag': 1,
                  'len': Math.min(data.length - (i + 1), max_len),
                  'data': data.slice(i, i + max_len)
                },
                function(message) {
                  this.send(message.buffer);
                }.bind(this));
            }
          }
          break;
        default:
          emit("gps.log.transmitter.error", "Invalid protocol " + this.o.protocol);
      }
    }.bind(this));

    return true;
  };

  transmitter.prototype.send = function(message) {
    if (this.serial !== undefined) {
      this.serial.write(message);
    }
  };

  transmitter.prototype.stop = function() {
    var running = false;
    if (this.serial !== undefined) {
      this.serial.close();
      this.serial = undefined;
      running = true;
    }
    if (this.rtcmlisten) {
      this.rtcmlisten.end();
      this.rtcmlisten = undefined;
      running = true;
    }
    return running;

  };

  transmitter.prototype.reload = function(options) {
    console.log("rtcm.reload");
    var running = this.stop();
    this.constructor(this.parent, options);
    if (running) {
      return this.start();
    }
  }

  return new transmitter((arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments)));
}
