window.addEventListener('load', function(e) {
  if (window.applicationCache) {
    window.applicationCache.addEventListener('updateready', function(e) {
        window.applicationCache.swapCache();
        window.location.reload();
    });
    window.applicationCache.update();
  }
});
