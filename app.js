const basestation = require('./modules/basestation')();

var consoleLog = console.log;
function trace(bool) {
    if (!!bool) {
      console.log = function() {
        console.trace.apply(null, arguments);
      };
    } else {
      console.log = consoleLog;
    }
}
function debug(bool) {
    if (!!bool) {
      console.log = consoleLog;
    } else {
      console.log = function() {};
    }
}
trace(false);
debug(false);

basestation.on("gps.log.listener.error", function() {
  console.log.apply(this, arguments);
});

basestation.on("gps.daemon.error", function() {
  console.log.apply(this, arguments);
});

basestation.on("gps.transmitter.error", function() {
  console.log.apply(this, arguments);
});

basestation.init();
