var ui = {
  "connected": false,
  "status": "",
  "mode": 0,
  "ppp": "txtPPP",
  "lon": 0,
  "lat": 0,
  "alt": 0,
  "sat": 0,
  "sending": 0,
  "fix": 0,
  "maxfix": 0,
  "lang": Cookies.get("lang") || "en",
  "reciever": "",
  "transmitter": "",
  "devices": [],
  "protocol": 0,
  "_protocol": 0,
  "protocols": [
    "RTCM 3", "MavLink"
  ]
};

var bindings = {};

var functions = {
  "selectReceiver": function() {
    ui._receiver = $(this).attr("data-elem");
  },
  "selectTransmitter": function() {
    ui._transmitter = $(this).attr("data-elem");
  },
  "selectProtocol": function() {
    console.log($(this).attr("data-elem"));
    ui._protocol = $(this).attr("data-elem");
  }
};

var strings = {
  "empty": "",
  "mode": "txtMode",
  "lon": "txtLon",
  "lat": "txtLat",
  "alt": "txtAlt",
  "status": "txtStatus",
  "mode0": "txtMode0",
  "mode1": "txtMode1",
  "settings": "txtSettings",
  "on": "txtOn",
  "off": "txtOff",
  "ok": "txtOk",
  "language": "txtLanguage",
  "wifipw": "txtWifiPw",
  "protocol": "txtProtocol",
  "currenttitle": "txtCurrentTitle",
  "locationtitle": "txtLocationTitle",
  "locationupdate": "txtLocationUpdate",
  "locationhint": "txtLocationHint",
  "devicestitle": "txtDevicesTitle",
  "devicepath": "txtDevicePath",
  "devicedriver": "txtDeviceDriver",
  "refresh": "txtRefresh",
  "ppptitle": "txtPPPTitle",
  "pppstart": "txtPPPStart",
  "pppstop": "txtPPPStop",
  "disconnected0": "txtDisconnectedTitle",
  "disconnected1": "txtDisconnectedMessage",
  "restarting0": "txtRestartingTitle",
  "restarting1": "txtRestartingMessage",
  "unavailablesettings": "txtUnavailableSettings",
  "noselection": "txtNoSelection",
  "receiver": "txtReceiver",
  "transmitter": "txtTransmitter",
  "errorStr2Str0": "txtErrorStr2StrTitle",
  "errorStr2Str1": "txtErrorStr2StrMessage",
  "str2strStop": "txtStr2StrStop",
  "str2strStart": "txtStr2StrStart",
  "str2strRestart": "txtStr2StrRestart",
};

var uiregex = new RegExp("{{([a-zA-Z0-9 _.:]*)}}", "g");

var app;

rivets.formatters.i18n = function(v) {
  return i18n(v);
};
rivets.formatters.var = function(v) {
  return String(v).replace(uiregex, function(m, c) {
    if (ui[c] != undefined) {
      return ui[c];
    }
    return m;
  });
};
rivets.formatters.coords = function(v) {
  return v + "°";
};
rivets.formatters.meters = function(v) {
  return v + " m";
};
rivets.binders.i18n = function(el, v) {
  var e = $(el);
  var rv = e.attr("data-rv");
  var base = ui[rv];
  if (base == undefined) {
    return;
  }
  var translated = i18n(base);
  var replaced = translated.replace(uiregex, function(m, c) {
    c = c.trim();
    if (c != undefined && ui[c] != undefined) {
      var prop = {};
      prop[c] = {
        get: function() {
          return ui["_" + c];
        },
        set: function (val) {
          ui["_" + c] = val;
          var b = bindings[c];
          if (b == undefined) {
            return;
          }
          for (var k in b) {
            if (b.hasOwnProperty(k) && ui[k] != undefined) {
              app.sync();
            }
          }
        }
      };
      if (!bindings[c]) {
        bindings[c] = {};
      }
      bindings[c][rv] = true;
      ui["_" + c] = ui[c];
      Object.defineProperties(ui, prop);
      return ui[c];
    }
    return m;
  });
  e.text(replaced);
};
function btnaction(el, v, act) {
  if (typeof v == "boolean") {
    v = v ? 1 : 0;
  }
  var e = $(el);
  var comp = e.attr("data-comp") || "";
  var m = comp + e.val();
  var v = (v == comp + "2") ? comp + "1" : v;
  var m = (m == comp + "2") ? comp + "1" : m;
  //console.log("e", e, "v", v, "c", comp, "m", m, undefined);
  act(e, v == m);
}
function btn_enabled(el, v) {
  return btnaction(el, v, function(e, s) {
    e.prop('disabled', !s);
  });
}
function btn_disabled(el, v) {
  return btnaction(el, v, function(e, s) {
    e.prop('disabled', s);
  });
}
function btn_class(c1, c2, el, v) {
  return btnaction(el, v, function(e, s) {
    if (s) {
      e.addClass(c1);
      e.removeClass(c2);
    } else {
      e.addClass(c2);
      e.removeClass(c1);
    }
  });
}
rivets.binders.btnprimary = function(el, v) {
  return btn_class("btn-primary", "btn-secondary", el, v);
};
rivets.binders.btnsuccess = function(el, v) {
  return btn_class("btn-success", "btn-secondary", el, v);
};
rivets.binders.btndanger = function(el, v) {
  return btn_class("btn-danger", "btn-secondary", el, v);
};
rivets.binders.btnenabled = function(el, v) {
  return btn_enabled(el, v);
};
rivets.binders.btndisabled = function(el, v) {
  return btn_disabled(el, v);
};
rivets.binders.langname = function(el, v) {
  var name = v;
  if (languages && languages[v] && languages[v].name) {
    name = languages[v].name;
  }
  $(el).text(name);
};
rivets.binders.langactive = function(el, v) {
  $(el).find("a.dropdown-item").each(function() {
    var e = $(this);
    if (e.attr("data-lang") == v) {
      e.addClass("bg-primary text-white");
    } else {
      e.removeClass("bg-primary text-white");
    }
  });
};
rivets.binders.grayout = function(el, v) {
  $(el)[v ? "removeClass" : "addClass"]("grayout");
};
rivets.binders.dropdown = function(el, v) {
  var text = "";
  var type = $(el).attr("data-type");
  if (type == "0" && v >= 0 && ui.devices[v] != undefined && ui.devices[v].name != undefined) {
    text = ui.devices[v].name;
  } else if (type == "1" && v >= 0 && ui.protocols[v] != undefined) {
    text = ui.protocols[v];
  } else {
    text = i18n(strings.noselection);
  }
  $(el).find(".dropdown-toggle").text(text);
  $(el).find(".dropdown-item").each(function() {
    var e = $(this);
    if (e.attr("data-elem") == v) {
      e.addClass("bg-primary text-white");
    } else {
      e.removeClass("bg-primary text-white");
    }
  });
};
rivets.binders.tooltip = function(el, v) {
  //$(el).attr("title", v);
  $(el).attr("data-original-title", v);
};
rivets.binders["hide-*"] = function(el, v) {
  $(el)[v ? "addClass" : "removeClass"]("hide-" + this.args[0]);
};
rivets.binders["show-*"] = function(el, v) {
  $(el)[!v ? "addClass" : "removeClass"]("hide-" + this.args[0]);
};
