var _i18n = {};
var languages = {
  "en": {
    "name": "English",
    "url": "i18n/en.json"
  },
  "de": {
    "name": "Deutsch",
    "url": "i18n/de.json"
  },
  "sv": {
    "name": "Svenska",
    "url": "i18n/sv.json"
  },
  "ru": {
    "name": "Русский",
    "url": "i18n/ru.json"
  }
}

function changeLanguage(lang) {
  if (languages[lang] != undefined) {
    ui.lang = lang;
    Cookies.set('lang', lang);
    $.getJSON(languages[lang].url, function(data) {
      _i18n = data;
      app.sync();
    });
  }
}

function i18n(v) {
  return _i18n[String(v)] || String(v);
}
