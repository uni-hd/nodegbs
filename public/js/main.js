$(document).ready(function() {

  app = rivets.bind($('#app'), { "_": ui, "$": functions, "!": strings });

  $(document).on("click", function(e) {
    if (!$(e.target).closest("#nav").length) {
      $(this).blur();
      var navbar = $("#navbar");
      if (!navbar.hasClass("collapsing")) {
        navbar.collapse('hide');
      }
    }
  });

  $("button, #selectLanguage a").on("click", function(e) {
    if (!$(e.target).closest("#nav .navbar-toggler").length) {
      $(this).blur();
      var navbar = $("#navbar");
      if (!navbar.hasClass("collapsing")) {
        navbar.collapse('hide');
      }
    }
  });

  $("#restoresettings").on("click", function() {
    console.log("restore");
    ui._receiver = ui.receiver;
    ui._transmitter = ui.transmitter;
    ui._protocol = ui.protocol;
    $("#wifipw").val("");
  });

  $("#savesettings").on("click", function() {
    // TODO: Check format of password
    if (socket != undefined) {
      var pw = $("#wifipw").val();
      if (pw != undefined && pw != "")
      socket.emit("update password", pw);
      ui.receiver = ui._receiver;
      var r = {
        "vendor": "",
        "model": "",
        "serial": ""
      }
      if (ui.receiver >= 0) {
        r = ui.devices[ui.receiver];
      }
      console.log(r);
      if (r != undefined) {
        socket.emit("select receiver", {
          "vendor": r.vendor,
          "model": r.model,
          "serial": r.serial
        });
      }
      ui.transmitter = ui._transmitter;
      var t = {
        "vendor": "",
        "model": "",
        "serial": ""
      }
      if (ui.transmitter >= 0) {
        t = ui.devices[ui.transmitter];
      }
      if (t != undefined) {
        socket.emit("select transmitter", {
          "vendor": t.vendor,
          "model": t.model,
          "serial": t.serial
        });
      }
      ui.protocol = ui._protocol;
      if (ui.protocol >= 0) {
        socket.emit("select protocol", ui.protocol);
      }
    }
  });

  $.each(languages, function(k, v) {
    var l = $("<a>");
    l.addClass("dropdown-item");
    l.attr("data-lang", k);
    l.text(v.name);
    $("#selectLanguage").append(l);
  });

  $("#selectLanguage a").on("click", function() {
    changeLanguage($(this).attr("data-lang"));
  });

  changeLanguage(ui.lang);

  $(".selectMode").on("click", function() {
    if (socket != undefined) {
      socket.emit("update mode", $(this).val());
    }
  });

  $(".selectSending").on("click", function() {
    if (socket != undefined) {
      socket.emit("update sending", $(this).val());
    }
    $(this).blur();
  });

  $(".selectLocation").on("click", function() {
    var loc = {
      "lon": $("#selectLon").val(),
      "lat": $("#selectLat").val(),
      "alt": $("#selectAlt").val()
    };
    console.log(loc);
    if (socket != undefined) {
      socket.emit("update location", loc);
    }
  });

  $(".hasTooltip").tooltip();

});
