var socket;
if (typeof io !== "undefined") {
  socket = io();
}

if (socket != undefined) {
  socket.on('connect', function() {
    ui.connected = true;
    socket.emit("update");
  });
}

if (socket != undefined) {
  socket.on('disconnect', function() {
    ui.connected = false;
  });
}

if (socket != undefined) {
  socket.on('restart', function() {
    ui.restarting = true;
  });
}

if (socket != undefined) {
  socket.on("update status", function(status) {
    console.log(status);
    $("#divLocation")[(status.mode == 0) ? "removeClass" : "addClass"]("hide");
    $("#divPPP")[(status.mode == 0) ? "addClass" : "removeClass"]("hide");
    $("#divPPPSet")[(status.mode == 1) ? "removeClass" : "addClass"]("hide");
    $("#divPPPHint")[(status.mode == 1) ? "addClass" : "removeClass"]("hide");
    ui.mode = "mode" + status.mode;
    ui.status = "status" + status.conn;
    ui.sat = status.sat;
    ui.maxfix = status.maxfix;
    ui.fix = status.fix;
  });
}

if (socket != undefined) {
  socket.on("update sending", function(state) {
    console.log(state);
    ui.sending = state;
  });
}

if (socket != undefined) {
  socket.on("update location", function(loc) {
    console.log(loc);
    $("#selectLon").val(ui.lon = loc.lon);
    $("#selectLat").val(ui.lat = loc.lat);
    $("#selectAlt").val(ui.alt = loc.alt);
  });
}

if (socket != undefined) {
	socket.on("update devices", function(data) {
		console.log("update devices", data);
    if (data == undefined) {
      return;
    }
    if (data.devices == undefined) {
      data.devices = [];
    }
    if (data.receiver == undefined) {
      data.receiver = -1;
    }
    if (data.transmitter == undefined) {
      data.transmitter = -1;
    }
		ui.devices = data.devices;
    ui.receiver = data.receiver;
    ui._receiver = data.receiver;
		ui.transmitter = data.transmitter;
		ui._transmitter = data.transmitter;
	});
}

if (socket != undefined) {
	socket.on("error str2str", function(data) {
		console.log("error occoured");
    $('#modalErrorStr2Str').modal("show");
	});
}

function toInt(v) {
  return parseInt(toFloat(v));
}

function toFloat(v) {
  if (typeof v === "string") {
    var f = parseFloat(v);
    return (f === NaN) ? 0 : f;
  }
  return v;
}
